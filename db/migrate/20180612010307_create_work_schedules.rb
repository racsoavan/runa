class CreateWorkSchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :work_schedules do |t|
      t.string :description
      t.datetime :start_time
      t.datetime :end_time
      t.datetime :schedule
      t.belongs_to :user, index: true, null: false

      t.timestamps
    end
  end
end
