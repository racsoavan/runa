Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post "login_user" => "home#login_user"
  get "sign_out" => "home#sign_out"
  get "download" => "home#download"
  get "calls" => "home#calls"
  namespace :admin do
    resources :users, :except => [:show] do
      collection do
        get :download
      end
    end
    root :to => "admin#index"    
  end
   namespace :api, defaults: {format: 'json'} do
    scope module: :v1 do
      resources :services do
        collection do
          post :access
          get :get_work_schedules
        end
      end
    end
  end
  root 'home#index'
end
