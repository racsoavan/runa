class Services
  def initialize(session)
    @session = session
  end
  def get_work_schedules()
    data_params = {}
    data_header = {'d-access-token' => @session[:current_user]["token_access"], "Content-Type" => "application/json"}
    data_response = call_services('api/services/get_work_schedules',data_params,data_header,'get')
  end
  def autenticate_user(params)
	  data_params = {'email' => params[:email], 'password': params[:password]}
    data_response = call_services('api/services/access',data_params)
  end
  def call_services(url,data_params,data_header={"Content-Type" => "application/json"},call_method='post')
    p "----------------#{url.inspect}"
  	auth_app = RestClient::Request.execute(method: call_method,
                            url: 'https://runa-app.herokuapp.com/'+url,
                            payload: data_params,
                            headers: data_header
                           )
  	JSON.parse(auth_app.body.as_json)
  end
end