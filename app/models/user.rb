class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  default_scope { order('id') } 
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :work_schedules
  accepts_nested_attributes_for :work_schedules, reject_if: :all_blank, allow_destroy: true
  def self.validate_access(params)
    admin = self.find_by_email(params[:email])
    if (admin && admin.valid_password?(params[:password]) && admin.admin == false) 
      admin.get_token_user
      r_a = admin
    else
      r_a = false
    end
    r_a
  end

  def get_token_user
    unless self.token_access
      self.token_access = SecureRandom.hex
      self.save
    end
    self.token_access
  end
end
