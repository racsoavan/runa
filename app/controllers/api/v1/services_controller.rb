module Api
  module V1
    class ServicesController < ActionController::Base
      respond_to :json
      def access
        @admin = User.validate_access(params)
        if @admin
          render json: @admin
        else
          render json: {error: "No fue posible autenticarte", status: :unprocessable_entity}
        end
      end
      def get_work_schedules
        admin = User.find_by_token_access(request.headers['d-access-token'])
        if admin
          render json: admin.work_schedules
        else
          render json: {error: "No fue posible autenticarte", status: :unprocessable_entity}
        end
      end
    end
  end
end
