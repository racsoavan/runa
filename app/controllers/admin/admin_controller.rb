class Admin::AdminController < AdminController
	def index
	  users = User.all
	  @admins = users.where(admin: true)
	  @users = users.where(admin: false)
	end
end
