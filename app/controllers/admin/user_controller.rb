class Admin::UsersController < AdminController
  require "csv"
  def index
  	@users = User.all
  end
  def new
  	@user = User.new
  end
  def create
    @user = User.new(user_params)
    @user.password = SecureRandom.urlsafe_base64
    if @user.save
      flash[:notice] = 'Usuario Creado'
      redirect_to :controller => "users" and return
    else
      flash[:notice] = 'Error Usuario'
      render action: "new" 
    end
  end
  def edit
  	@user = User.find params[:id]
  end 
   def update
  	@user = User.find(params[:id])

    if params[:user][:password].blank?
      params[:user].delete(:password)
    end
    if @user.update(user_params)
      flash[:notice] = 'Usuario Modificado'
      redirect_to :controller => "users" and return
    else
      flash[:notice] = 'Error Usuario'
      render action: "edit" 
    end
  end
  def download 
    body = ['Nombre','Correo','Administrador','Telefono','Entradas']
    body = body.to_csv
    User.all.each do |user|
      row = [user.name, user.email,user.admin ? 'Administrador' : 'Usuario',user.phone,user.work_schedules.map { |u| "(#{u.schedule.strftime("%d/%m/%Y")}-#{u.start_time.strftime("%H:%M:%S")}-#{u.end_time.strftime("%H:%M:%S")})" }.join(",")]
      body << row.to_csv
    end
    send_data body, filename: 'usuarios.csv'
  end

  def destroy
  	raise params.inspect
    @course.destroy
    flash[:notice] = 'Course Deleted'
    redirect_to :controller => "users" and return
  end
  private

    def user_params
      params.require(:user).permit(:name, :email, :phone, :admin, :password,
       				 work_schedules_attributes: [:id, :schedule, :start_time, :end_time, :_destroy])
    end
end
