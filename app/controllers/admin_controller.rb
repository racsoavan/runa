class AdminController < ApplicationController
	before_action :authenticate_user!
	before_action :validate_admin
    layout 'admin'
	private
	def validate_admin
	  if !current_user.admin?
	  	sign_out(current_user)
	    redirect_to root_path 
	  end
	end
end
