class HomeController < ApplicationController
	require "csv"
	before_action :load_info, only: [:login_user, :index, :download]
	def login_user
	  admin = @services.autenticate_user(params)
	  @error = "Error al registrarse"
	  unless admin["status"] == "unprocessable_entity"
	  	session[:current_user] = admin
	  	redirect_to root_url and return
	  end
	end
	def sign_out
		session[:current_user] = nil
	  	redirect_to root_url and return
	end
	def download
	  	@ws = @services.get_work_schedules

	    body = [session[:current_user]["name"],session[:current_user]["email"],session[:current_user]["phone"]]
	    body = body.to_csv
	    body << ['Fecha','Hora de Entrada','Hora de salida'].to_csv
	    @ws.each do |ws|
	      row = [ws["schedule"].to_date.strftime("%d/%m/%Y"),ws["start_time"].to_time.strftime("%H:%M:%S"),ws["end_time"].to_time.strftime("%H:%M:%S")]
	      body << row.to_csv
	    end
	    send_data body, filename: "#{session[:current_user]["name"]}.csv"
	end
	def index
 	  @ws = session[:current_user].nil? ? [] : @services.get_work_schedules
	end
    private
    def load_info
      @services = Services.new(session)
    end
end
