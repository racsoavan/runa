class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  private
  def after_sign_in_path_for(resource)
    if resource.admin?
      admin_root_url
    else
      root_url
    end
  end
end
