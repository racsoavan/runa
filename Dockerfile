FROM ruby:2.3.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /runa
WORKDIR /runa
COPY Gemfile /runa/Gemfile
COPY Gemfile.lock /runa/Gemfile.lock
RUN bundle install
COPY . /runa